import streamlit as st
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
import glob, os
from datetime import datetime

def incidencia_bar(x, y):
    fig_m_prog = go.Figure([go.Bar(x=x, y=y, text=y, textposition='auto')])
    fig_m_prog.update_layout(paper_bgcolor="#fbfff0", plot_bgcolor="#fbfff0",
                            font={'color': "#008080", 'family': "Arial"}, height=100, width=300,
                            margin=dict(l=15, r=1, b=4, t=4))
    fig_m_prog.update_yaxes(title='y', visible=False, showticklabels=False)
    fig_m_prog.update_traces(marker_color='#17A2B8', selector=dict(type='bar'))
    return fig_m_prog

def pie_chart_incidencia(df_group, title, value):
    fig = px.pie(df_group, title=title, values=value, names=df_group.index)
    fig.update_layout(height=300, width=500)
    return fig

def read_xlsx_files(s_date, e_date):
    files_list = []
    path = 'data/'
    files = os.listdir(path)
    for file in files:
        ext = file.split('.')
        if ('xlsx' in ext):
            name_file = os.path.splitext(file)[0]
            if len(name_file.split('ReporteReco04')) >= 2:
                sp = name_file.split('ReporteReco04')
                if len(sp[1]) == 10 and sp[0] == '':
                    date_time_obj = datetime.strptime(sp[1], '%Y-%m-%d')
                    date_time_obj = datetime.date(date_time_obj)
                    flag_s = date_time_obj >= s_date
                    flag_e = date_time_obj <= e_date
                    if(flag_s and flag_e):
                        files_list.append("data/" + file)
                    # files_list.append(pd.read_excel("data/" + file))
                    # files_list.append(file)
    return files_list
    # if len(files_list) > 0:
    #     return merge_xlsx_files(files_list)
    # else:
    #     return -1

def merge_xlsx_files(files_list):
    files = []
    for f in files_list:
        files.append(pd.read_excel(f))
    merge = pd.concat(files)
    merge.to_excel("data/MergeAllData.xlsx", index=False)
    return pd.concat(files)

# ---
excel_file = 'data/ReporteReco042021-06-30.xlsx'
sheet_name = 'ControlesReco4'
df = pd.read_excel(excel_file, sheet_name=sheet_name, header=0, index_col=[0])
xl = pd.ExcelFile(excel_file)
sheets_name = xl.sheet_names

tipo_incidencia = df['Tipo_de_Incidencia'].unique().tolist()
df['data_date_part'] = pd.to_datetime(df['data_date_part']).dt.date

control_names = ['0', '11', '12', '13', '14', '15', '16', '17', '18', '19',
    '20', '21', '22', '23', '24', '25', '30', '31', '32', '33', '34', '35',
    '40', '50', '60', '61', '70']

# init conf
st.set_page_config(page_title='Dashboard Santander', page_icon="📊",
                   layout='wide', initial_sidebar_state='auto')

# app sidebar
st.sidebar.header("DASHBOARD SATANDER")
st.sidebar.subheader("Navegation")
nav = st.sidebar.radio("", ["Dahsboard", "Resumen", "Page 3"])
st.sidebar.subheader("Account")

# pages
if nav == "Dahsboard":
    st.header("Home")
    col1, col2, col3 = st.beta_columns((2, 1, 1))
    with col2:
        s_date = st.date_input("Desde")
    with col3:
        e_date = st.date_input("Hasta")
    with col1:
        "Rango de fechas:", s_date, ' - ', e_date

    list_xlsx = read_xlsx_files(s_date, e_date)
    if len(list_xlsx) > 0:
        df = merge_xlsx_files(list_xlsx)
    st.write(df)
    df['data_date_part'] = pd.to_datetime(df['data_date_part']).dt.date
    df_range = df[(df['data_date_part'].between(s_date, e_date))]
    controles = df_range.drop_duplicates(subset = ["ID_CONTROL"])
    controles = controles[['ID_CONTROL', 'DESCRIPCION']]
    
    #############
    ##Perimetro
    #############
    st.subheader("Perímetro")
    da_p = df_range[['Perimetro', 'data_date_part']].groupby("data_date_part").mean()
    fig = px.scatter(da_p, labels={"data_date_part":"Fecha", "value": "Perímetro"})
    fig.update_layout(xaxis_title="Fecha", yaxis_title="Perímetro")
    fig.update_traces(marker=dict(size=12, color='crimson', line=dict(width=2, color='DarkSlateGrey')), selector=dict(mode='markers'))
    st.write(fig)
    #############

    #############
    ## Incidencias
    #############
    st.subheader("Tipo de incidencias")
    tipo_incidencia_selection = st.selectbox('Incidencias:', options=tipo_incidencia)
    #############
    titulo = False
    for i, control in controles.iterrows():
        c = control['ID_CONTROL']
        desc = control['DESCRIPCION']
        name_s = "control_" + c.split("_")[1]
        mask1 = (df_range['Tipo_de_Incidencia'] == (tipo_incidencia_selection)) & (df_range['ID_CONTROL'] == (c))
        data = df_range[mask1]
        if len(data):
            output_columns = ['Operaciones_Sivasa_ampliado', 'IT_Sivasa_Ampliado', 'Diferencias', 'data_date_part']
            data_chart = data[output_columns]
            y1 = data_chart['Operaciones_Sivasa_ampliado']
            y2 = data_chart['IT_Sivasa_Ampliado']
            y3 =data_chart['Diferencias']
            with st.beta_container():
                if y1.sum() > 0 or y2.sum() > 0 or y3.sum() > 0:
                    st.subheader(f'Control ({c.split("_")[1]}): {desc}')
                    titulo = True
                    x = data_chart['data_date_part']
                    col1_1, col1_2, col1_3 = st.beta_columns(3)
                    with col1_1:
                        y = data_chart['Operaciones_Sivasa_ampliado']
                        if(y.sum() > 0 ):
                            # break
                            st.write(f'{y.sum()} Operaciones_Sivasa_ampliado')
                            st.plotly_chart(incidencia_bar(x, y))
                    with col1_2:
                        y = data_chart['IT_Sivasa_Ampliado']
                        if(y.sum()>0 ):
                            # break
                            st.write(f'{y.sum()} IT_Sivasa_Ampliado')
                            st.plotly_chart(incidencia_bar(x, y))
                    with col1_3:
                        y = data_chart['Diferencias']
                        if(y.sum() > 0 ):
                            # break
                            st.write(f'{y.sum()} Diferencias')
                            st.plotly_chart(incidencia_bar(x, y))
            if y1.sum() > 0 or y2.sum() > 0 or y3.sum() > 0:
                list_xlsx_control = []
                for file_xlsx in list_xlsx:
                    # st.write(file_xlsx)
                    xl = pd.ExcelFile(file_xlsx)
                    sheets_name = xl.sheet_names
                    if (name_s in sheets_name):
                        list_xlsx_control.append(pd.read_excel(file_xlsx, sheet_name=name_s, header=0, index_col=[0]))
                if (titulo == False):
                    st.subheader(f'Control ({c.split("_")[1]}): {desc}')
                df2 = pd.concat(list_xlsx_control)
                # df2 = pd.read_excel(excel_file, sheet_name=name_s, header=0, index_col=[0])
                df2 = df2.fillna('')
                df2['data_date_part'] = pd.to_datetime(df2['data_date_part']).dt.date
                df2_range = df2[(df2['data_date_part'].between(s_date, e_date))]
                colf_1, colf_2 = st.beta_columns((2, 1))
                with colf_1:
                    st.write(df2_range)
                with colf_2:
                    df_finalidad = df2_range.groupby(by=['desc_finalidad']).count()[['cod_finalidad']]
                    fig = px.pie(df_finalidad, title='Finalidad', values='cod_finalidad', names=df_finalidad.index)
                    fig.update_layout(height=300, width=500)
                    st.plotly_chart(fig)
            titulo = False

if nav == "Resumen":
    st.header("Resumen")
    st.write(df)

if nav == "Page 3":
    st.header("Subtitle Page 3")
